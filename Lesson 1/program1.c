/**
 * For this file we are going to create a "helloworld" application, except there
 * is no real hello world in robotc. What we will do instead is move a motor.
 * If you already have your cortex, please get it, a motor controller, some wire,
 * and one of the motor interface cable/terminal pairs.
 * 
 * For now, we won't be crimping anything because you can only crimp something once.
 * Granted, if you needed to you could make your trip down to the nearest radioshack
 * to buy more crimps, but unfortunately they are few and far between, so instead,
 * save them until you build your actual robot.
 **/
 
 //In robotc we use task main{} as the primary code for our robot.
 task main{
     motor[port3] = vexRT(Ch3);
 }
 