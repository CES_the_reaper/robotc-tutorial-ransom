
     ,-----.,--.                  ,--. ,---.   ,--.,------.  ,------.
    '  .--./|  | ,---. ,--.,--. ,-|  || o   \  |  ||  .-.  \ |  .---'
    |  |    |  || .-. ||  ||  |' .-. |`..'  |  |  ||  |  \  :|  `--, 
    '  '--'\|  |' '-' ''  ''  '\ `-' | .'  /   |  ||  '--'  /|  `---.
     `-----'`--' `---'  `----'  `---'  `--'    `--'`-------' `------'
    ----------------------------------------------------------------- 


Welcome to my RobotC tutorial space, granted you have found it. Although it is being taught to ransom middle school, I encourage you to look through it. I will try to make it as readable as possible.

Code will look like this: 
```
#!c
task main{
    motor[port3] = vexRT(Ch2);
}
```
There is a really good tutorial on how to start programming at <http://cdn.robotc.net/pdfs/vex/curriculum/Fundamentals%20-%20ROBOTC%20Programming.pdf>